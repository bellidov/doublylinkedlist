package ru.niit.mylist;


public class MyComparable implements Comparable<MyComparable>
{
	private String data;
	public MyComparable(String data)
	{
		this.data = data;
	}
	
	@Override
	public int compareTo(MyComparable o) {
		// TODO Auto-generated method stub
		return this.data.compareTo(o.toString());
	}
	
	public String toString(){
		return data;
	}
	
	public boolean equals(Object o){
		if(o == this) return true;
		if(!(o instanceof MyComparable)) return false;
		
		MyComparable mc = (MyComparable)o;
		
		return mc.toString().equals(data);
	}

}