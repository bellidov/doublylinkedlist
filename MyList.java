package ru.niit.mylist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class MyList<T extends Comparable<T>> implements List<T>
{
	private Node<T> first;
	private Node<T> last;
	private int size = 0;
	
	public MyList()	{
	}
	
	@Override
	public boolean add(T e) {
		Node<T> newNode = new Node(e, last, null);
		
//		(first == null) ? (first = newNode) : (last.next = newNode);
		if(first == null){
			first = newNode;
		}
		else{
			last.next = newNode;
		}

		last = newNode;
		size++;
		return true;
	}
	
	@Override
	public T get(int index) {
		return getNodeByIndex(index).obj;
	}
	
	private Node<T> getNodeByIndex(int index){
		Node<T> temp;

		if(index >=0 && index < size/2){ 
			temp = first;
			for(int i = 0; i < index; i++){
				temp = temp.next;
			}
			return temp;
		}
		else if(index >= size/2 && index < size){
			temp = last;
			for(int i = size-1; i > index; i--){
				temp = temp.prev;
			}
			return temp;
		}
		else return null;
	}

	@Override
	public int size() {
		return this.size;
	}
	
	
	public boolean equals(Object o){ // si
		if(o == this) return true;
		if(!(o instanceof MyList)) return false;
		
		MyList ml = (MyList)o;
		
		boolean temp = true; 
		for(int i = 0; i < size; i++){ // sravnivayu vse elementi spiska
			temp = temp && ml.get(i).equals(get(i));
		}
		
		return temp;
	}
	
	@Override
	public void add(int index, T element) {
		if(index == 0){
			Node<T> newNode = new Node(element, null, first);
			first.prev = newNode;
			first = newNode;
		}
		else if(index == size){
			this.add(element);
			size--;
		}
		else if(index > 0 && index < size){
			Node<T> temp = getNodeByIndex(index);
			Node<T> newNode = new Node(element, temp.prev, temp);
			temp.prev.next = newNode;
			temp.prev = newNode;
		}
		else size--; // chtoby kompensirovat rost kotoriy budet nizhe;
		size++;
	}
	
	@Override
	public T set(int index, T element) { // si
		// TODO Auto-generated method stub
		
		if(index > 0 && index < size-1){
			Node<T> temp = getNodeByIndex(index);
			Node<T> newNode = new Node<T>(element, temp.prev, temp.next);
			temp.prev.next = newNode;
			temp.next.prev = newNode;
			return temp.obj;
		}
		else if(index == 0){
			
			Node<T> temp = getNodeByIndex(index);
			Node<T> newNode = new Node<T>(element, null, temp.next);
			temp.next.prev = newNode;
			first = newNode;
			return temp.obj;
		}
		else if(index == size-1){
			Node<T> temp = getNodeByIndex(index);
			Node<T> newNode = new Node<T>(element, temp.prev, null);
			temp.prev.next = newNode;
			last = newNode;
			return temp.obj;
		}
		else return null;
	}
	

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub si
		for(Node<T> nod = first; nod != null;){
			Node<T> tempNext = nod.next;
			nod.obj = null;
			nod.prev = null;
			nod.next = null;
			nod = tempNext;
		}
		first = last = null;
		size = 0;		
	}
	
	public String toString(){
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < size; i++) {
		    s.append(this.getNodeByIndex(i).obj);
		    s.append("\n");
		}
		
		return s.toString();
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int indexOf(Object o) { // 
		// TODO Auto-generated method stub
		T temp = (T)o; //??
		for(int i = 0; i < size; i++){
			if(o.equals(getNodeByIndex(i).obj)){
				return i;
			}
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub 
		T temp = (T)o; //??
		for(int i = size-1; i >= 0; i--){
			if(o.equals(getNodeByIndex(i).obj)){
				return i;
			}
		}
		return -1;
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub no
		return null;
	}
	
	

	@Override
	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub no
		
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub no
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		
		return null;
	}
	
	private class Node<E>
	{
		Node<E> prev; 
		Node<E> next;
		E obj;
		
		public Node(E obj, Node<E> prev, Node<E> next){
			this.obj = obj;
			this.prev = prev;
			this.next = next;
		}
	}

}
